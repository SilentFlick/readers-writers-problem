#include "readers_writers.h"

int main() {
  pid_t pid, son;
  createSemaphores();
  createFile();
  openSemaphore(mutex);
  openSemaphore(w);
  // create writers
  for (int i = 0; i < NUMBEROFWRITERS; i++) {
    if ((son = fork()) == 0) {
      writer();
    } else if (son == -1) {
      handler_error("fork");
    }
  }
  // create readers
  for (int i = 0; i < NUMBEROFREADERS; i++) {
    if ((son = fork()) == 0) {
      reader();
    } else if (son == -1) {
      handler_error("fork");
    }
  }
  // wait for childs
  while ((pid = wait(&ret)) > 0) {
    printf("Son PID [%ld] returned %d.\n", (long)pid, ret);
  }
  removeSemaphore(mutex);
  removeSemaphore(w);
  removeSemaphore(rc);
  return 0;
}
