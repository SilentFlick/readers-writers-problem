#include "readers_writers.h"

int main() {
  pthread_t son[NUMBEROFREADERS + NUMBEROFWRITERS + 1];
  pthread_attr_t attr;
  int i;
  createFile();
  ret = pthread_attr_init(&attr);
  if (ret != 0) {
    handler_error_en(ret, "pthread_attr_init");
  }
  ret = pthread_attr_setstacksize(&attr, STACKSIZE);
  if (ret != 0) {
    handler_error_en(ret, "pthread_attr_setstacksize");
  }
  for (i = 0; i < NUMBEROFWRITERS; i++) {
    ret = pthread_create(&son[i], &attr, &pthread_writer, NULL);
    if (ret != 0) {
      handler_error_en(ret, "pthread_create, writer");
    }
  }
  for (; i < NUMBEROFREADERS + NUMBEROFWRITERS; i++) {
    ret = pthread_create(&son[i], &attr, &pthread_reader, NULL);
    if (ret != 0) {
      handler_error_en(ret, "pthread_create, reader");
    }
  }
  ret = pthread_attr_destroy(&attr);
  if (ret != 0) {
    handler_error_en(ret, "pthread_attr_destroy");
  }
  for (i = 0; i < NUMBEROFREADERS + NUMBEROFWRITERS; i++) {
    (void)pthread_join(son[i], NULL);
    printf("Thread %d TID [%x] returned %d.\n", i, (unsigned int)son[i], ret);
  }
  exit(EXIT_SUCCESS);
}
