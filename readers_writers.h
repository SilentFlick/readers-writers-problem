#ifndef READERS_WRITERS_H_
#define READERS_WRITERS_H_

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// Semaphore
#define SEMKEYID 75
#define SEMKEYID_W 76
#define SEMKEYID_RC 77
#define SEMKEYPATH "/dev/null"
#define NUMSEMS 1
#define SEM_MUTEX 0
#define RESSOURCE "zahl.dat"
#define NUMBEROFWRITERS 5
#define NUMBEROFREADERS 5
#define ITER 20000
#define handler_error_en(en, msg)                                              \
  do {                                                                         \
    errno = en;                                                                \
    perror(msg);                                                               \
    exit(EXIT_FAILURE);                                                        \
  } while (0)
#define handler_error(msg)                                                     \
  do {                                                                         \
    perror(msg);                                                               \
    exit(EXIT_FAILURE);                                                        \
  } while (0)

int mutex, w, rc;
int ret;
key_t semkey_mutex, semkey_w, semkey_rc;
union semun {
  int val;
  struct semid_ds *buf;
  unsigned short *array;
  struct seminfo *__buf;
} mysemun;

void createFile() {
  FILE *f = fopen(RESSOURCE, "w");
  if (f == NULL) {
    handler_error("fopen");
  }
  fprintf(f, "0");
  fclose(f);
}

void writeToFile() {
  FILE *f = fopen(RESSOURCE, "r+");
  if (f) {
    unsigned long number;
    fscanf(f, "%ld", &number);
    number++;
    rewind(f);
    fprintf(f, "%ld\n", number);
    fclose(f);
  } else {
    handler_error("fopen");
  }
}

void readFile() {
  unsigned long number;
  FILE *f = fopen(RESSOURCE, "r");
  if (f) {
    ret = fscanf(f, "%lu", &number);
    if (ret != 1) {
      handler_error("fscanf");
    }
    fclose(f);
  }
}

void P(int semid) {
  struct sembuf P = {0, -1, 0};
  ret = semop(semid, &P, 1);
  if (ret == -1) {
    handler_error("semop P");
  }
}

void V(int semid) {
  struct sembuf V = {0, 1, 0};
  ret = semop(semid, &V, 1);
  if (ret == -1) {
    handler_error("semop V");
  }
}

void createSemaphores() {
  semkey_mutex = ftok(SEMKEYPATH, SEMKEYID);
  if (semkey_mutex == (key_t)-1) {
    handler_error("ftok semkey_mutex");
  }
  semkey_w = ftok(SEMKEYPATH, SEMKEYID_W);
  if (semkey_w == (key_t)-1) {
    handler_error("ftok semkey_w");
  }
  semkey_rc = ftok(SEMKEYPATH, SEMKEYID_RC);
  if (semkey_rc == (key_t)-1) {
    handler_error("ftok semkey_rc");
  }
  mutex = semget(semkey_mutex, NUMSEMS, 0666 | IPC_CREAT | IPC_EXCL);
  if (mutex == -1) {
    handler_error("semget mutex");
  }
  w = semget(semkey_w, NUMSEMS, 0666 | IPC_CREAT | IPC_EXCL);
  if (w == -1) {
    handler_error("semget w");
  }
  rc = semget(semkey_rc, NUMSEMS, 0666 | IPC_CREAT | IPC_EXCL);
  if (w == -1) {
    handler_error("semget rc");
  }
}

void removeSemaphore(int semid) {
  ret = semctl(semid, SEM_MUTEX, IPC_RMID, mysemun);
  if (ret == -1) {
    handler_error("semctl");
  }
  printf("Removed semaphor %d\n", semid);
}

void openSemaphore(int semid) {
  mysemun.val = 1;
  ret = semctl(semid, SEM_MUTEX, SETVAL, mysemun);
  if (ret == -1) {
    removeSemaphore(semid);
    handler_error("semctl");
  }
}

void writer() {
  int c;
  printf("Writer PID [%d] starts.\n", getpid());
  for (c = 0; c < ITER; c++) {
    P(w);
    writeToFile();
    V(w);
  }
  printf("Writer PID [%d] finished.\n", getpid());
  exit(EXIT_SUCCESS);
}

void reader() {
  int c;
  union semun rsemun;
  printf("Reader PID [%d] starts.\n", getpid());
  for (c = 0; c < ITER; c++) {
    P(mutex);
    // rc++
    ret = semctl(rc, 0, GETVAL);
    if (ret == -1) {
      handler_error("semctl rc, GETVAL rc++");
    }
    rsemun.val = ret + 1;
    ret = semctl(rc, 0, SETVAL, rsemun);
    if (ret == -1) {
      handler_error("semctl rc, SETVAL rc++");
    }
    // if(rc == 1)
    if (rsemun.val == 1) {
      P(w);
    }
    V(mutex);
    readFile();
    P(mutex);
    // rc--
    ret = semctl(rc, 0, GETVAL);
    if (ret == -1) {
      handler_error("semctl rc, GETVAL rc--");
    }
    rsemun.val = ret - 1;
    ret = semctl(rc, 0, SETVAL, rsemun);
    if (ret == -1) {
      handler_error("semctl rc, SETVAL rc--");
    }
    // if(rc == 0)
    if (rsemun.val == 0) {
      V(w);
    }
    V(mutex);
  }
  printf("Reader PID [%d] finished.\n", getpid());
  exit(EXIT_SUCCESS);
}

// pthreads
#define STACKSIZE 65536
pthread_mutex_t pmutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t pw = PTHREAD_MUTEX_INITIALIZER;
int prc;

void *pthread_writer(void *arg) {
  int c;
  printf("Writer PID [%d] starts.\n", getpid());
  for (c = 0; c < ITER; c++) {
    pthread_mutex_lock(&pw);
    writeToFile();
    pthread_mutex_unlock(&pw);
  }
  printf("Writer PID [%d] finished.\n", getpid());
  pthread_exit(NULL);
}

void *pthread_reader(void *arg) {
  int c;
  printf("Reader PID [%d] starts.\n", getpid());
  for (c = 0; c < ITER; c++) {
    pthread_mutex_lock(&pmutex);
    rc++;
    if (rc == 1) {
      pthread_mutex_lock(&pw);
    }
    pthread_mutex_unlock(&pmutex);
    readFile();
    pthread_mutex_lock(&pmutex);
    rc--;
    if (rc == 0) {
      pthread_mutex_unlock(&pw);
    }
    pthread_mutex_unlock(&pmutex);
  }
  printf("Reader PID [%d] finished.\n", getpid());
  pthread_exit(NULL);
}
#endif // READERS_WRITERS_H_
